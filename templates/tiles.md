{# templates/tiles.md #}
# {{ tile_name }}

```{article-info}
:date: "{{ modification_date }}"
```

{% for level in titles %}

::::{grid} 2
:gutter: 1

:::{grid-item-card} {{ '<br />- or -<br />'.join(level[2].split(';')) }}
:text-align: center 
{octicon}`person`

:::
:::{grid-item-card} {{ '<br />- or -<br />'.join(level[3].split(';')) }}
:text-align: center
{octicon}`people`
:::
::::

:::::{dropdown} Level {{level[0] }} - {{ level[1] }}
:animate: fade-in-slide-down
:color: info
:margin: 0

::::{tab-set}
:::{tab-item} Overview

{% for item in overview[level[0]] %}
- {{ item }}
{% endfor %}
:::
:::{tab-item} Responsibilities

{% for item in responsibilities[level[0]] %}
- {{ item }}
{% endfor %}
:::
:::{tab-item} Qualifications

{% for item in qualifications[level[0]] %}
- {{ item }}
{% endfor %}
:::
:::{tab-item} Skills

{% for item in skills[level[0]] %}
- {{ item }}
{% endfor %}
:::
:::{tab-item} Team Lead

{% for item in team_lead[level[0]] %}
- {{ item }}
{% endfor %}
:::
::::

:::::

---

{% endfor %}

[Home](home)