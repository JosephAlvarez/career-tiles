import os

for root, dirs, files in os.walk('.'):
    for filename in files:
        filename = os.path.join(root, filename)

        if filename.endswith('.html'):

            file_data = None

            with open(filename, 'r') as old_file:
                file_data = old_file.read()
            
            new_file_data = file_data.replace('.html"', '.aspx"')

            new_filename = filename.replace('.html', '.aspx')

            with open(new_filename, 'w') as new_file:
                new_file.write(new_file_data)
            
            os.remove(filename)