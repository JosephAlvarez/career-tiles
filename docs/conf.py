from datetime import datetime

# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))

# -- Project information -----------------------------------------------------

now = datetime.now()
start_year = 2022
organization = 'Altair Engineering'
owner = organization
author = organization
project = 'Altair Engineer Career Tiles Reference Guide'
maintainer = [
    'LaRue Brown <lbrown@altair.com>',
    'Joey Mack <jmack@altair.com>',
]

# The full version, including alpha/beta/rc tags
release = 'alpha'
version = f'{now.year}.{now.month}.{now.day}-{now.hour}{now.minute}{now.second}'
show_authors = True
copyright = f'{start_year}, {organization}' \
    if now.year == start_year \
    else f'{start_year} - {now.year}, {organization}'

# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'sphinx_design', 
    'myst_parser',
    'sphinx.ext.autodoc'
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = [
    '_build', 
    'Thumbs.db', 
    '.DS_Store'
]

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'sphinx_rtd_theme'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
#html_static_path = ['_static']
html_logo = 'images/logo.png'

html_theme_options = {
    'body_max_width': 'auto', 
    'logo_only': True,
    'display_version': False,
    'collapse_navigation': False,
    'navigation_depth': 3
}

# MyST syntax extensions
myst_enable_extensions = [
    'amsmath',
    'colon_fence',
    'deflist',
    'dollarmath',
    'fieldlist',
    'html_admonition',
    'html_image',
    'linkify',
    'replacements',
    'smartquotes',
    'strikethrough',
    'substitution',
    'tasklist'
]