(home)=
# Altair Career Tiles Reference Guide

   The first step to getting somewhere is to decide you’re not going to stay where you are. 
   --John Pierpoint Morgan

Career Tiles are grouping of career paths to inform and guide potential career discussions, transfer/move decisions, and recruitment discussions. 

* Career Tiles Educates and informs employees about potential career paths, capabilities & skills applicable within each tile + level 
* Provides a framework to help employees progress towards their career goals and objectives, helps retain our key talent 
* Improves employer brand during recruiting 


```{toctree}
---
maxdepth: 1
caption: Contents
glob:
---

*/index
```