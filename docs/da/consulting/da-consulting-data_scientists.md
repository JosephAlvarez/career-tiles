
# Data Scientists

```{article-info}
:date: "February 27, 2023"
```



::::{grid} 2
:gutter: 1

:::{grid-item-card} Intern
:text-align: center 
{octicon}`person`

:::
:::{grid-item-card} None
:text-align: center
{octicon}`people`
:::
::::

:::::{dropdown} Level 1 - Student
:animate: fade-in-slide-down
:color: info
:margin: 0

::::{tab-set}
:::{tab-item} Overview


- Works under supervision of a data analytics mentor

- works on meaningful projects for a defined duration

:::
:::{tab-item} Responsibilities


- Knowledge transfer of any new or emerging technology learned in school

- Hygiene of CRM, SFDC, and demo environment(s)

- Complete assigned projects

:::
:::{tab-item} Qualifications


- Academic relevance in one or more of the following: sciences, mathematics, statistics, computer science, or engineering

- Any level of experience in machine learning

- A strong desire to explore the world of data analytics

- Suggested experience: Currently studying towards Bachelor, Master, or PhD

:::
:::{tab-item} Skills


- For in-depth technical skill matrix, refer to the appendix

:::
:::{tab-item} Team Lead


- Not applicable

:::
::::

:::::

---



::::{grid} 2
:gutter: 1

:::{grid-item-card} Data Scientist
:text-align: center 
{octicon}`person`

:::
:::{grid-item-card} None
:text-align: center
{octicon}`people`
:::
::::

:::::{dropdown} Level 2 - Staff
:animate: fade-in-slide-down
:color: info
:margin: 0

::::{tab-set}
:::{tab-item} Overview


- Works independently on tasks assigned by manager

- Focused on a single family of products at a time

- Expected to be learning products and developing skills on the job

:::
:::{tab-item} Responsibilities


- Excellent presentation & communication skills, both web and in-person

- Help customers with Altair Data Analytics products by: answering questions, assisting with installations, troubleshooting issues, and providing workarounds (field-developed solutions)

- Develop a deep understanding of Altair Data Analytics products and dependent technologies

- Ability to work on multiple priorities simultaneously

- Monitoring and responding to end user inquiries via telephone, web, chat, email and online forums

- Creating Knowledge-base articles (FAQ/Solutions) that provide technical value to the Altair Data Analytics customer base

- Conduct Altair product presentations and demonstrations, which include, but are not limited to question-and-answer discussions with the customer’s technical staff 

- Contribute to technical training and support 

:::
:::{tab-item} Qualifications


- Bachelor's, Master's or PhD in Computer Science, Engineering, Mathematics, Data Science or a related technical field 

- Exposure/Knowledge of Data Analytics tools

- Driven individual, excited to learn new technology

- Strong Interpersonal and communication skills (oral and written)

- Team player with ability to work independently as well as collaboratively

- Relevant Internship experience is a plus

- Ability to travel to client locations for projects or as appropriate

- Suggested experience: Previous intern in data science, 0-2 years as a data scientist

:::
:::{tab-item} Skills


- For in-depth technical skill matrix, refer to the appendix

:::
:::{tab-item} Team Lead


- Not applicable

:::
::::

:::::

---



::::{grid} 2
:gutter: 1

:::{grid-item-card} Sr. Data Scientist
:text-align: center 
{octicon}`person`

:::
:::{grid-item-card} None
:text-align: center
{octicon}`people`
:::
::::

:::::{dropdown} Level 3 - Senior
:animate: fade-in-slide-down
:color: info
:margin: 0

::::{tab-set}
:::{tab-item} Overview


- Increased knowledge of multiple products either in depth or width 

- Gain an understanding of the solutions we can provide for engineering problems and the product interdependencies for it

- Ability to work on more than one functional area or product

- Contributes to solution direction

:::
:::{tab-item} Responsibilities


- Provide discovery support during initial stages of sales cycle

- Provide vignettes, context and color commentary when exploring a selling opportunities

- Execute projects independently

- Contribute to best practices

- Excellent at troubleshooting product-related issues, fixing or requesting support to fix issues

- Ramp up on multiple products/solutions in Altair’s suite and contribute to the completion of scheduled tasks within assigned technical engagements with minimal supervision 

- Contribute to technical trainings and support in multiple products and advanced topics 

- Help in technical documentation in the form of digital content, knowledge base articles and be active on the Community forums of Altair 

- Test the products for stability and validation, especially with regards to usability and workflows for solutions to common problems in the industry 

- Gain a deeper understanding of Altair’s products and their applications and be able to work on proof of concepts and technical benchmarks 

- Propose Altair’s Support Services and Consulting Services to customers who need assistance to resolve challenging problems and build solutions of demanding complexity 

- When necessary be the main technical point of contact for the customer

:::
:::{tab-item} Qualifications


- Bachelor's, Master's or PhD in Computer Science, Engineering, Mathematics, Data Science or a related technical field 

- Good understanding of multiple products in Altair’s offerings

- Exposure/Knowledge of DA tools and the workflows in different industry verticals

- Exposure to other data analytics solutions/products (open-source, Alteryx, RapidMiner, Dataiku, etc.) is preferred

- Ability to collaborate with all levels of technical users and sales teams

- Driven individual, excited to learn new technology

- Strong Interpersonal and communication skills (oral and written)

- Team player with ability to work independently as well as collaboratively

- Suggested experience: 2 or more years as a data scientist

:::
:::{tab-item} Skills


- For in-depth technical skill matrix, refer to the appendix

:::
:::{tab-item} Team Lead


- Not applicable

:::
::::

:::::

---



::::{grid} 2
:gutter: 1

:::{grid-item-card} Lead Data Scientist
:text-align: center 
{octicon}`person`

:::
:::{grid-item-card} Team Lead - Data Science
:text-align: center
{octicon}`people`
:::
::::

:::::{dropdown} Level 4 - Lead
:animate: fade-in-slide-down
:color: info
:margin: 0

::::{tab-set}
:::{tab-item} Overview


- Increased knowledge of single or multiple products/offerings, expert in either a single product or a module, or good knowledge of multiple products/offerings

- Good understanding of the engineering problems we can provide solutions to and the product interdependencies

- Ability to work on more than one functional area, product, and solutions

- Create and contributes to solution direction

- Gain understanding of our products and solutions as used in the industry

- Gain an understanding of the best practices in a product/solution or industry

:::
:::{tab-item} Responsibilities


- Mentor, train, and guide interns/ junior team members 

- Contribute to technical trainings on advanced topics/products/solutions 

- Help in technical documentation in the form of digital content, knowledge base articles and be active on Altair community 

- Coordinate and collaborate smoothly across multiple internal and external cross- functional teams 

- Define work plans and project timings and own the technical aspects of a project

:::
:::{tab-item} Qualifications


- Bachelor's, Master's or PhD in Computer Science, Engineering, Mathematics, Data Science or a related technical field 

- Good understanding of multiple products in Altair’s offerings

- Exposure/Knowledge of DA tools and the workflows in different industry verticals

- Exposure to other data analytics solutions/products (open-source, Alteryx, RapidMiner, Dataiku, etc.) is preferred

- Ability to collaborate with all levels of technical users and sales teams

- Driven individual, excited to learn new technology

- Strong Interpersonal and communication skills (oral and written)

- Team player with ability to work independently as well as collaboratively

- Suggested experience: 4 or more years as a data scientist

:::
:::{tab-item} Skills


- For in-depth technical skill matrix, refer to the appendix

:::
:::{tab-item} Team Lead


- Demonstrated ability to lead teams 

- Attract and assist in hiring data scientists 

- Take part in the hiring process, assist in the on-boarding of new employees 

- Work on multiple technical engagements, with varying deadlines, and deliverables and prioritize them for the team 

:::
::::

:::::

---



::::{grid} 2
:gutter: 1

:::{grid-item-card} Specialist Data Scientist<br />- or -<br />Sr. Specialist Data Scientist
:text-align: center 
{octicon}`person`

:::
:::{grid-item-card} Manager - Data Science<br />- or -<br />Sr. Manager - Data Science
:text-align: center
{octicon}`people`
:::
::::

:::::{dropdown} Level 5 - Manager
:animate: fade-in-slide-down
:color: info
:margin: 0

::::{tab-set}
:::{tab-item} Overview


- Increased knowledge multiple products/our offerings, expert in multiple products/modules 

- In-depth understanding of the solutions we can provide and the product interdependencies in the technical area of interest 

- Good understanding of our products and solutions as used in the industry 

- Input in defining solutions and driving technical direction in customer engagements 

- Ability to work on more than one functional area, product, and solutions 

- Gain knowledge of the value of our product offerings and how to position them 

- Input in defining the best practices/solutions in an industry, publish papers/articles on the same 

- Gain understanding of upcoming areas in different industry verticals and be able to conceptualize forward thinking solutions 

- Communicates progress to various stake holders 

- Lead and manage a world-class team by hiring exceptional talent, coaching, and developing engineers, and leveraging your technical expertise to constantly prioritize project activities for the team 

- Manages technical resources between various project activities 

- Responsible for developing and tracking timelines for project activities and the ROI on the activities

:::
:::{tab-item} Responsibilities


- Work in tandem with project managers and account managers to define technical scope and deliverables

- Ability to set/manage customer expectations with regards to risk, deliverables, timeline and define the project execution strategy accordingly

- Develop project quotes with a focus on margins and profitability

:::
:::{tab-item} Qualifications


- Bachelor's, Master's or PhD in Computer Science, Engineering, Mathematics, Data Science or a related technical field 

- Good understanding of multiple products in Altair’s offerings

- Exposure/Knowledge of DA tools and the workflows in different industry verticals

- Exposure to other data analytics solutions/products (open-source, Alteryx, RapidMiner, Dataiku, etc.) is preferred

- Ability to collaborate with all levels of technical users and sales teams

- Driven individual, excited to learn new technology

- Strong Interpersonal and communication skills (oral and written)

- Team player with ability to work independently as well as collaboratively

- Suggested experience: 6 or more years as a data scientist; 0 or more years managing data scientists

:::
:::{tab-item} Skills


- For in-depth technical skill matrix, refer to the appendix

:::
:::{tab-item} Team Lead


- Demonstrated ability to manage data scientists both technically and administratively

- Attract and hire data scientists 

- Define the needs of the members of the team and the areas of personnel development for them 

- Work on multiple technical engagements, with varying deadlines, and deliverables and prioritize them for the team 

- Will have data scientists as direct reports

:::
::::

:::::

---



::::{grid} 2
:gutter: 1

:::{grid-item-card} Principal Data Scientist<br />- or -<br />Sr. Principal Data Scientist
:text-align: center 
{octicon}`person`

:::
:::{grid-item-card} Director - Data Science<br />- or -<br />Sr. Director - Data Science
:text-align: center
{octicon}`people`
:::
::::

:::::{dropdown} Level 6 - Director
:animate: fade-in-slide-down
:color: info
:margin: 0

::::{tab-set}
:::{tab-item} Overview


- Increased knowledge multiple products/our offerings, expert in multiple products/modules 

- In-depth understanding of the solutions we can provide and the product interdependencies in the technical area of interest 

- Good understanding of our products and solutions as used in the industry 

- Input in defining solutions and driving technical direction in customer engagements 

- Ability to work on more than one functional area, product, and solutions 

- Gain knowledge of the value of our product offerings and how to position them 

- Input in defining the best practices/solutions in an industry, publish papers/articles on the same 

- Gain understanding of upcoming areas in different industry verticals and be able to conceptualize forward thinking solutions 

- Communicates progress to various stake holders 

- Lead and manage a world-class team by hiring exceptional talent, coaching, and developing engineers, and leveraging your technical expertise to constantly prioritize project activities for the team 

- Manages technical resources between various project activities 

- Responsible for developing and tracking timelines for project activities and the ROI on the activities 

:::
:::{tab-item} Responsibilities


- Assists Project Managers in defining resources needs, workplan, project deliverables, timing for advanced projects 

- Encourages adoption and application of new technologies and generation of technical content 

- Ability to set/manage customer expectations with regards to risk, deliverables, timeline and define the project execution strategy accordingly 

- Develop project quotes with a focus on margins and profitability, while also working in tandem with the AM’s and ATT to push our solutions covering current and future horizons at strategic customers 

:::
:::{tab-item} Qualifications


- Bachelor's, Master's or PhD in Computer Science, Engineering, Mathematics, Data Science or a related technical field 

- Good understanding of multiple products in Altair’s offerings

- Exposure/Knowledge of DA tools and the workflows in different industry verticals

- Exposure to other data analytics solutions/products (open-source, Alteryx, RapidMiner, Dataiku, etc.) is preferred

- Ability to collaborate with all levels of technical users and sales teams

- Driven individual, excited to learn new technology

- Strong Interpersonal and communication skills (oral and written)

- Team player with ability to work independently as well as collaboratively

- Suggested experience: 8 or more years as a data scientist; 4 or more years managing data scientists

:::
:::{tab-item} Skills


- For in-depth technical skill matrix, refer to the appendix

:::
:::{tab-item} Team Lead


- (Senior) Managers will report to (Senior) Directors

- Significant oversight of the resource management and is a key stakeholder in cost and budget control processes 

- Takes ownership of team productivity; remove roadblocks and identify and implement improvements to our processes and procedures 

- Coach and develop managers and data scientists on your team for maximum impact and results 

- Leads multiple project teams, diverse and/or global – working on different technology areas, customer teams or platforms 

- Effectively coach, guide and mentor team managers and employees to recognize and further performance and potential 

- Have a feel on the pulse of the team

:::
::::

:::::

---



::::{grid} 2
:gutter: 1

:::{grid-item-card} Distinguished Data Scientist
:text-align: center 
{octicon}`person`

:::
:::{grid-item-card} Vice President - Data Analytics Consulting
:text-align: center
{octicon}`people`
:::
::::

:::::{dropdown} Level 7 - Vice President
:animate: fade-in-slide-down
:color: info
:margin: 0

::::{tab-set}
:::{tab-item} Overview


- Manage one or more product areas, or significant components of technology 

- Senior leader of a world-class, global team by hiring exceptional talent, coaching, and developing data scientists/data engineers, and leveraging your technical expertise to constantly improvise 

- Provides vision and long-term goals to the overall vision of data analytics consulting 

- Develops a strategic technical vision to determine how the teams will contribute to the company's vision by spearheading transformational services offerings 

- Provides internal and external communication on data analytics consulting services 

- Establish standards and adopt effective best practices 

- Makes strategic workforce planning, staffing and people decisions to ensure the teams stay on course 

- Key stakeholder in preparation, management, and optimization of budget 

- Socialize new ideas from your team, drive the effort of turning innovations to impactful products

:::
:::{tab-item} Responsibilities


- Knowledgeable of market, industries, competition, and customers and how Altair products can influence market trends and evolution 

- Analyzes customer’s current and future needs. In collaboration with Sales and Field teams, influences the conversion of current/unknown customer problems into new technology, sales opportunities, and/or consulting engagements 

- Envisions market and customer evolution to inform product lines and new offerings 

- Review results and status of projects/team deliverables and ensure seamless communication to leadership and customers 

- Recruits externally, networking with interesting candidates to strengthen the talent pipeline

:::
:::{tab-item} Qualifications


- Bachelor's, Master's or PhD in Computer Science, Engineering, Mathematics, Data Science or a related technical field 

- Strong strategic and business acumen

- Expert in one or more industry domains and technology/product lines

- Exposure/Knowledge of DA tools and the workflows in different industry verticals

- Exposure to other data analytics solutions/products (open-source, Alteryx, RapidMiner, Dataiku, etc.) is preferred

- Team player with ability to work independently as well as collaboratively

- Suggested experience: 10 or more years as a data scientist; 8 or more years leading data scientists/data engineers

:::
:::{tab-item} Skills


- For in-depth technical skill matrix, refer to the appendix

:::
:::{tab-item} Team Lead


- (Senior) Directors will report to VP 

- Lead the implementation of staffing plan, factoring in technical decisions and professional development. Ability to do workforce planning with a global lens 

- “Player and coach” who innovates and inspires the team successfully 

- Demonstrated success as a leader in a fast-paced environment and collaborate across global teams, including Sales and Consulting 

:::
::::

:::::

---



::::{grid} 2
:gutter: 1

:::{grid-item-card} None
:text-align: center 
{octicon}`person`

:::
:::{grid-item-card} Sr. Vice President - Data Analytics Consulting
:text-align: center
{octicon}`people`
:::
::::

:::::{dropdown} Level 8 - Sr. Vice President
:animate: fade-in-slide-down
:color: info
:margin: 0

::::{tab-set}
:::{tab-item} Overview


- Manage one or more product areas, or significant components of technology 

- Senior leader of a world-class, global team by hiring exceptional talent, coaching, and developing data scientists/data engineers, and leveraging your technical expertise to constantly improvise 

- Provides vision and long-term goals to the overall vision of data analytics consulting 

- Develops a strategic technical vision to determine how the teams will contribute to the company's vision by spearheading transformational services offerings 

- Provides internal and external communication on data analytics consulting services 

- Establish standards and adopt effective best practices 

- Senior leader of a world-class, global team by hiring exceptional talent, coaching, and developing engineers, and leveraging your technical expertise to constantly improvise 

- Develops a strategic technical vision to determine how the team/s will contribute to the company’s vision by spearheading transformational consulting and solution offerings 

- Makes strategic workforce planning, staffing and people decisions to ensure the teams stay on course 

- Key stakeholder in preparation, management, and optimization of budget 

- Socialize new ideas from your team, drive the effort of turning innovations to impactful products

:::
:::{tab-item} Responsibilities


- Knowledgeable of market, industries, competition, and customers and how Altair products can influence market trends and evolution 

- Analyzes customer’s current and future needs. In collaboration with Sales and Field teams, influences the conversion of current/unknown customer problems into new technology, sales opportunities, and/or consulting engagements 

- Envisions market and customer evolution to inform product lines and new offerings 

- Review results and status of projects/team deliverables and ensure seamless communication to leadership and customers 

- Recruits externally, networking with interesting candidates to strengthen the talent pipeline

:::
:::{tab-item} Qualifications


- Bachelor's, Master's or PhD in Computer Science, Engineering, Mathematics, Data Science or a related technical field 

- Strong strategic and business acumen

- Expert in one or more industry domains and technology/product lines

- Technology expert who actively contributes and participates in bringing awareness and strengthening Altair's brand through conferences, external events, publications, and academia

- Exposure/Knowledge of DA tools and the workflows in different industry verticals

- Exposure to other data analytics solutions/products (open-source, Alteryx, RapidMiner, Dataiku, etc.) is preferred

- Suggested experience: 15 or more years as a data scientist; 10 or more years leading data scientists/data engineers

:::
:::{tab-item} Skills


- For in-depth technical skill matrix, refer to the appendix

:::
:::{tab-item} Team Lead


- VP and Senior Directors will report to Sr. VP 

- Lead the implementation of staffing plan, factoring in technical decisions and professional development. Ability to do workforce planning with a global lens 

- “Player and coach” who innovates and inspires the team successfully 

- Demonstrated success as a leader in a fast-paced environment and collaborate across global teams, including Sales and Consulting 

:::
::::

:::::

---



[Home](home)