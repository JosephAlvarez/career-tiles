
# Pre-Sales

```{article-info}
:date: "February 27, 2023"
```



::::{grid} 2
:gutter: 1

:::{grid-item-card} Intern
:text-align: center 
{octicon}`person`

:::
:::{grid-item-card} None
:text-align: center
{octicon}`people`
:::
::::

:::::{dropdown} Level 1 - Student
:animate: fade-in-slide-down
:color: info
:margin: 0

::::{tab-set}
:::{tab-item} Overview


- Works under supervision of a data analytics mentor

- works on meaningful projects for a defined duration

:::
:::{tab-item} Responsibilities


- Knowledge transfer of any new or emerging technology learned in school

- Hygiene of CRM, SFDC, and demo environment(s)

- Complete assigned projects

:::
:::{tab-item} Qualifications


- Academic relevance in one or more of the following: sciences, mathematics, statistics, computer science, or engineering

- Any level of experience in machine learning

- A strong desire to explore the world of data analytics

:::
:::{tab-item} Skills


- For in-depth technical skill matrix, refer to the appendix

:::
:::{tab-item} Team Lead


- Not applicable

:::
::::

:::::

---



::::{grid} 2
:gutter: 1

:::{grid-item-card} Solution Specialist
:text-align: center 
{octicon}`person`

:::
:::{grid-item-card} None
:text-align: center
{octicon}`people`
:::
::::

:::::{dropdown} Level 2 - Staff
:animate: fade-in-slide-down
:color: info
:margin: 0

::::{tab-set}
:::{tab-item} Overview


- Works independently on tasks assigned by manager

- Focused on a single family of products at a time

- Expected to be learning products and developing skills on the job

:::
:::{tab-item} Responsibilities


- Excellent presentation & communication skills, both web and in-person

- Help customers with Altair Data Analytics products by: answering questions, assisting with installations, troubleshooting issues, and providing workarounds (field-developed solutions)

- Develop a deep understanding of Altair Data Analytics products and dependent technologies

- Ability to work on multiple priorities simultaneously

- Monitoring and responding to end user inquiries via telephone, web, chat, email and online forums

- Creating Knowledge-base articles (FAQ/Solutions) that provide technical value to the Altair Data Analytics customer base

- Conduct Altair product presentations and demonstrations, which include, but are not limited to question-and-answer discussions with the customer’s technical staff

- Contribute to technical training and support

:::
:::{tab-item} Qualifications


- Bachelor’s, Master’s or PhD in Computer Science, Engineering, Mathematics, Data Science or a related technical field

- Exposure/Knowledge of Data Analytics tools

- Driven individual, excited to learn new technology

- Strong Interpersonal and communication skills (oral and written)

- Team player with ability to work independently as well as collaboratively

- Relevant Internship experience is a plus

- Ability to travel to client locations for projects or as appropriate

:::
:::{tab-item} Skills


- For in-depth technical skill matrix, refer to the appendix

:::
:::{tab-item} Team Lead


- Not applicable

:::
::::

:::::

---



::::{grid} 2
:gutter: 1

:::{grid-item-card} Sr. Solution Specialist
:text-align: center 
{octicon}`person`

:::
:::{grid-item-card} None
:text-align: center
{octicon}`people`
:::
::::

:::::{dropdown} Level 3 - Senior
:animate: fade-in-slide-down
:color: info
:margin: 0

::::{tab-set}
:::{tab-item} Overview


- Increased knowledge of multiple products either in depth or width

- Gain an understanding of the solutions we can provide for engineering problems and the product interdependencies for it

- Ability to work on more than one functional area or product

- Contributes to solution direction

:::
:::{tab-item} Responsibilities


- Provide discovery support during initial stages of sales cycle

- Provide vignettes, context and color commentary when exploring a selling opportunities

- Execute projects independently

- Contribute to best practices

- Excellent at troubleshooting product-related issues, fixing or requesting support to fix issues

- Ramp up on multiple products/solutions in Altair’s suite and contribute to the completion of scheduled tasks within assigned technical engagements with minimal supervision

- Contribute to technical trainings and support in multiple products and advanced topics

- Help in technical documentation in the form of digital content, knowledge base articles and be active on the Community forums of Altair

- Test the products for stability and validation, especially with regards to usability and workflows for solutions to common problems in the industry

- Gain a deeper understanding of Altair’s products and their applications and be able to work on proof of concepts and technical benchmarks

- Propose Altair’s Support Services and Consulting Services to customers who need assistance to resolve challenging problems and build solutions of demanding complexity

- Strong understanding of data science and machine learning concepts

- General understanding of distributed compute and cloud architecture

- General understanding of each account and opportunity status

- Proactively maintain contact with contacts at customer accounts

- Able to present a standard pitch briefing (first meeting presentation) to customers as needed

- Conduct Altair product presentations and demonstrations, which include, but not limited to question-and-answer discussions with the customer’s technical staff

:::
:::{tab-item} Qualifications


- Bachelor’s, Master’s or PhD in Computer Science, Engineering, Mathematics, Data Science or a related technical field

- Good understanding of multiple products in Altair’s offerings

- Exposure/Knowledge of DA tools and the workflows in different industry verticals

- Exposure to other data analytics solutions/products (open-source, Alteryx, RapidMiner, Dataiku, etc.) is preferred

- Ability to collaborate with all levels of technical users and sales teams

- Driven individual, excited to learn new technology

- Strong Interpersonal and communication skills (oral and written)

- Team player with ability to work independently as well as collaboratively

:::
:::{tab-item} Skills


- For in-depth technical skill matrix, refer to the appendix

:::
:::{tab-item} Team Lead


- Not applicable

:::
::::

:::::

---



::::{grid} 2
:gutter: 1

:::{grid-item-card} Lead Solution Specialist
:text-align: center 
{octicon}`person`

:::
:::{grid-item-card} Team Lead - Solution Specialist
:text-align: center
{octicon}`people`
:::
::::

:::::{dropdown} Level 4 - Lead
:animate: fade-in-slide-down
:color: info
:margin: 0

::::{tab-set}
:::{tab-item} Overview


- Increased knowledge of single or multiple products/offerings, expert in either a single product or a module, or good knowledge of multiple products/offerings

- Good understanding of the engineering problems we can provide solutions to and the product interdependencies

- Ability to work on more than one functional area, product, and solutions

- Create and contributes to solution direction

- Gain understanding of our products and solutions as used in the industry

- Gain an understanding of the best practices in a product/solution or industry

:::
:::{tab-item} Responsibilities


- Mentor, train, and guide interns/ junior team members

- Contribute to technical trainings on advanced topics/products/solutions

- Help in technical documentation in the form of digital content, knowledge base articles and be active on Altair community

- Coordinate and collaborate smoothly across multiple internal and external cross- functional teams

:::
:::{tab-item} Qualifications


- Bachelor’s, Master’s or PhD in Computer Science, Engineering, Mathematics, Data Science or a related technical field

- Good understanding of multiple products in Altair’s offerings

- Exposure/Knowledge of DA tools and the workflows in different industry verticals

- Exposure to other data analytics solutions/products (open-source, Alteryx, RapidMiner, Dataiku, etc.) is preferred

- Ability to collaborate with all levels of technical users and sales teams

- Driven individual, excited to learn new technology

- Strong Interpersonal and communication skills (oral and written)

- Team player with ability to work independently as well as collaboratively

:::
:::{tab-item} Skills


- For in-depth technical skill matrix, refer to the appendix

:::
:::{tab-item} Team Lead


- Demonstrated ability to lead teams

- Attract and assist in hiring Solution Specialists

- Define the needs of the members of the team and the areas of personnel development for them

- Build a talent pipeline and take part in the hiring process, own the on-boarding of new employees

- Work on multiple technical engagements, with varying deadlines, and deliverables and prioritize them for the team

:::
::::

:::::

---



::::{grid} 2
:gutter: 1

:::{grid-item-card} Solution Architect<br />- or -<br />Sr. Solution Architect
:text-align: center 
{octicon}`person`

:::
:::{grid-item-card} Manager - Solution Specialist<br />- or -<br />Sr. Manager - Solution Specialist
:text-align: center
{octicon}`people`
:::
::::

:::::{dropdown} Level 5 - Manager
:animate: fade-in-slide-down
:color: info
:margin: 0

::::{tab-set}
:::{tab-item} Overview


- Increased knowledge multiple products/our offerings, expert in multiple products/modules

- In-depth understanding of the solutions we can provide and the product interdependencies in the technical area of interest

- Good understanding of our products and solutions as used in the industry

- Input in defining solutions and driving technical direction in customer engagements

- Ability to work on more than one functional area, product, and solutions

- Gain knowledge of the value of our product offerings and how to position them

- Input in defining the best practices/solutions in an industry, publish papers/articles on the same

- Gain understanding of upcoming areas in different industry verticals and be able to conceptualize forward thinking solutions

- Communicates progress to various stake holders

- Lead and manage a world-class team by hiring exceptional talent, coaching, and developing engineers, and leveraging your technical expertise to constantly prioritize project activities for the team

- Manages technical resources between various project activities

- Responsible for developing and tracking timelines for project activities and the ROI on the activities

:::
:::{tab-item} Responsibilities


- TBD

:::
:::{tab-item} Qualifications


- TBD

:::
:::{tab-item} Skills


- For in-depth technical skill matrix, refer to the appendix

:::
:::{tab-item} Team Lead


- TBD

:::
::::

:::::

---



::::{grid} 2
:gutter: 1

:::{grid-item-card} Principal Solution Architect<br />- or -<br />Sr. Principal Solution Architect
:text-align: center 
{octicon}`person`

:::
:::{grid-item-card} Director - Solution Specialists<br />- or -<br />Sr. Director - Solution Specialsits
:text-align: center
{octicon}`people`
:::
::::

:::::{dropdown} Level 6 - Director
:animate: fade-in-slide-down
:color: info
:margin: 0

::::{tab-set}
:::{tab-item} Overview


- Increased knowledge multiple products/our offerings, expert in multiple products/modules

- In-depth understanding of the solutions we can provide and the product interdependencies in the technical area of interest

- Good understanding of our products and solutions as used in the industry

- Input in defining solutions and driving technical direction in customer engagements

- Ability to work on more than one functional area, product, and solutions

- Gain knowledge of the value of our product offerings and how to position them

- Input in defining the best practices/solutions in an industry, publish papers/articles on the same

- Gain understanding of upcoming areas in different industry verticals and be able to conceptualize forward thinking solutions

- Communicates progress to various stake holders

- Lead and manage a world-class team by hiring exceptional talent, coaching, and developing engineers, and leveraging your technical expertise to constantly prioritize project activities for the team

- Manages technical resources between various project activities

- Responsible for developing and tracking timelines for project activities and the ROI on the activities

:::
:::{tab-item} Responsibilities


- TBD

:::
:::{tab-item} Qualifications


- TBD

:::
:::{tab-item} Skills


- For in-depth technical skill matrix, refer to the appendix

:::
:::{tab-item} Team Lead


- TBD

:::
::::

:::::

---



::::{grid} 2
:gutter: 1

:::{grid-item-card} Technical Fellow
:text-align: center 
{octicon}`person`

:::
:::{grid-item-card} Vice President - Global Technical Team
:text-align: center
{octicon}`people`
:::
::::

:::::{dropdown} Level 7 - Vice President
:animate: fade-in-slide-down
:color: info
:margin: 0

::::{tab-set}
:::{tab-item} Overview


- TBD

:::
:::{tab-item} Responsibilities


- TBD

:::
:::{tab-item} Qualifications


- TBD

:::
:::{tab-item} Skills


- For in-depth technical skill matrix, refer to the appendix

:::
:::{tab-item} Team Lead


- TBD

:::
::::

:::::

---



::::{grid} 2
:gutter: 1

:::{grid-item-card} None
:text-align: center 
{octicon}`person`

:::
:::{grid-item-card} Sr. Vice President -Global Technical Team
:text-align: center
{octicon}`people`
:::
::::

:::::{dropdown} Level 8 - Sr. Vice President
:animate: fade-in-slide-down
:color: info
:margin: 0

::::{tab-set}
:::{tab-item} Overview


- TBD

:::
:::{tab-item} Responsibilities


- TBD

:::
:::{tab-item} Qualifications


- TBD

:::
:::{tab-item} Skills


- For in-depth technical skill matrix, refer to the appendix

:::
:::{tab-item} Team Lead


- TBD

:::
::::

:::::

---



[Home](home)