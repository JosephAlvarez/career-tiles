
# Technical Support

```{article-info}
:date: "February 27, 2023"
```



::::{grid} 2
:gutter: 1

:::{grid-item-card} Intern
:text-align: center 
{octicon}`person`

:::
:::{grid-item-card} None
:text-align: center
{octicon}`people`
:::
::::

:::::{dropdown} Level 1 - Student
:animate: fade-in-slide-down
:color: info
:margin: 0

::::{tab-set}
:::{tab-item} Overview


- Works under supervision of a data analytics mentor

- Works on meaningful projects for a defined duration

:::
:::{tab-item} Responsibilities


- Knowledge transfer of any new or emerging technology learned in school

- Hygiene of CRM, SFDC, and demo environment(s)

- Complete assigned projects

:::
:::{tab-item} Qualifications


- Academic relevance in one or more of the following: sciences, mathematics, statistics, computer science, or engineering

- Any level of experience in machine learning

- A strong desire to explore the world of data analytics

:::
:::{tab-item} Skills


- For in-depth technical skill matrix, refer to the appendix

:::
:::{tab-item} Team Lead


- Not applicable

:::
::::

:::::

---



::::{grid} 2
:gutter: 1

:::{grid-item-card} Technical Support Engineer
:text-align: center 
{octicon}`person`

:::
:::{grid-item-card} None
:text-align: center
{octicon}`people`
:::
::::

:::::{dropdown} Level 2 - Staff
:animate: fade-in-slide-down
:color: info
:margin: 0

::::{tab-set}
:::{tab-item} Overview


- Works independently on tasks assigned by manager

- Focused on a single family of products at a time

- Expected to be learning products and developing skills on the job

:::
:::{tab-item} Responsibilities


- Excellent presentation & communication skills, both web and in-person

- Help customers with Altair Data Analytics products by: answering questions, assisting with installations, troubleshooting issues, and providing workarounds (field-developed solutions)

- Develop a deep understanding of Altair Data Analytics products and dependent technologies

- Ability to work on multiple priorities simultaneously

- Monitoring and responding to end user inquiries via telephone, web, chat, email and online forums

- Creating Knowledge-base articles (FAQ/Solutions) that provide technical value to the Altair Data Analytics customer base

- Conduct Altair product presentations and demonstrations, which include, but are not limited to question-and-answer discussions with the customer’s technical staff 

- Contribute to technical training and support 

- Provide support to Altair Data Analytics products that allows end users to perform in-depth analysis on their data while injecting predictive and complex statistics in the analysis.

- Manage and deliver Data Intelligence activities ranging from installation through project management and delivery.

- Identify and solve technical and/or software issues, define system architecture, and analyze and implement hardware/software specifications.

- Implement, test and provide guidance on solution deployments.

- Work with team members and managers to monitor the overall quality and timeliness of project delivery.

- Gather and provide project cost estimates, and prepare proposals and estimate the cost of Altair Data Analytics solution deliverables

:::
:::{tab-item} Qualifications


- Bachelor's, Master's or PhD in Computer Science, Engineering, Mathematics, Data Science or a related technical field 

- Exposure/Knowledge of Data Analytics tools

- Driven individual, excited to learn new technology

- Strong Interpersonal and communication skills (oral and written)

- Team player with ability to work independently as well as collaboratively

- Relevant Internship experience is a plus

-  Ability to travel to client locations for projects or as appropriate

- Two (2) + years of experience in the field of Data Intelligence/Data Science

- Bachelor's degree or equivalent in Information Systems, Engineering, Computer Science or related field

:::
:::{tab-item} Skills


- For in-depth technical skill matrix, refer to the appendix

- Strong analytic and troubleshooting skills 

- Strong Windows and Linux OS troubleshooting 

- Experience working with SQL and databases such SQL Server, Oracle and Postgres 

- Knowledge of Authentication mechanism like LDAP, Active Directory and Kerberos 

- Knowledge of Client/Server Web Application architecture 

- Experience with Virtual environments 

- Knowledge in Big Data Technologies 

- Knowledge of cloud technologies like Azure, AWS, GCP, Oracle is a plus

- Good Linux system administrator skills and TCP/IP network fundamentals

- Understanding of Cloud Computing technologies like AWS/Azure/GCP and/or Docker/Kubernetes is a plus

- Ability to produce clear, detailed project scoping and design documentation to be used by technical specialists to build appropriate solutions for Altair Knowledge Works customers

- Understanding of generic administration tasks of managing Docker images, container networking and standard infrastructure maintenance tasks on Docker and Kubernetes platform is a plus

- Professional Services/Consulting and client facing experience

:::
:::{tab-item} Team Lead


- Not applicable

:::
::::

:::::

---



::::{grid} 2
:gutter: 1

:::{grid-item-card} Sr. Technical Support Engineer
:text-align: center 
{octicon}`person`

:::
:::{grid-item-card} None
:text-align: center
{octicon}`people`
:::
::::

:::::{dropdown} Level 3 - Senior
:animate: fade-in-slide-down
:color: info
:margin: 0

::::{tab-set}
:::{tab-item} Overview


- Increased knowledge of multiple products either in depth or width

- Gain an understanding of the solutions we can provide for engineering problems and the product interdependencies for it

- Ability to work on more than one functional area or product

- Contributes to solution direction

:::
:::{tab-item} Responsibilities


- Provide discovery support during initial stages of sales cycle

- Provide vignettes, context and color commentary when exploring a selling opportunities

- Execute projects independently

- Contribute to best practices

- Excellent at troubleshooting product-related issues, fixing or requesting support to fix issues

- Ramp up on multiple products/solutions in Altair’s suite and contribute to the completion of scheduled tasks within assigned technical engagements with minimal supervision

- Contribute to technical trainings and support in multiple products and advanced topics

- Help in technical documentation in the form of digital content, knowledge base articles and be active on the Community forums of Altair

- Test the products for stability and validation, especially with regards to usability and workflows for solutions to common problems in the industry

- Gain a deeper understanding of Altair’s products and their applications and be able to work on proof of concepts and technical benchmarks

- Propose Altair’s Support Services and Consulting Services to customers who need assistance to resolve challenging problems and build solutions of demanding complexity

- Helping our customers be successful with Altair Data Analytics products by answering questions, assisting with installations, troubleshooting issues and providing workarounds.

- Identifying possible solutions to a variety of technical problems and takes appropriate action to resolve quickly.

- Quickly develop a deep understanding of Altair Data Analytics products and dependent technologies.

- Creating Knowledgebase articles (FAQ/Solutions) that provide technical value to the Altair Data Analytics customer base.

- Ability to work on multiple priorities simultaneously.

- Monitoring and responding to end user inquiries via telephone, web, chat, email and online forums.

- Excellent communication skills, especially telephone skills

- Mentoring junior level engineers on support processes and problem analysis skillset.

- Provide support to Altair Data Analytics products that allows end users to perform in-depth analysis on their data while injecting predictive and complex statistics in the analysis.

- Manage and deliver Data Analytics activities ranging from installation through project management and delivery.

- Identify and solve technical and/or software issues, define system architecture, and analyze and implement hardware/software specifications.

- Implement, test and provide guidance on solution deployments.

- Work with team members and managers to monitor the overall quality and timeliness of project delivery.

- Gather and provide project cost estimates, and prepare proposals and estimate the cost of Altair Data Analytics solution deliverables

:::
:::{tab-item} Qualifications


- Bachelor's, Master's or PhD in Computer Science, Engineering, Mathematics, Data Science or a related technical field

- Good understanding of multiple products in Altair’s offerings

- Exposure/Knowledge of DA tools and the workflows in different industry verticals

- Exposure to other data analytics solutions/products (open-source, Alteryx, RapidMiner, Dataiku, etc.) is preferred

- Ability to collaborate with all levels of technical users and sales teams

- Driven individual, excited to learn new technology

- Strong Interpersonal and communication skills (oral and written)

- Team player with ability to work independently as well as collaboratively

- Two (2) + years of experience in the field of Data Intelligence/Data Science

- Bachelor's/Masters degree or equivalent in Information Systems, Engineering, Computer Science or related field

- Ability to travel on a project basis to client locations

- Excellent communication skills, both written and oral

:::
:::{tab-item} Skills


- For in-depth technical skill matrix, refer to the appendix

- Strong analytic and troubleshooting skills

- Strong Windows and Linux OS troubleshooting

- Experience working with SQL and databases such SQL Server, Oracle and Postgres

- Excellent Knowledge of Authentication mechanism like LDAP, Active Directory and Kerberos

- Excellent Knowledge of Client/Server Web Application architecture

- Experience with Virtual environments is a must

- Excellent Knowledge in Big Data Technologies

- Excellent Knowledge of cloud technologies like Azure, AWS, GCP, Oracle

- Good Linux system administrator skills and TCP/IP network fundamentals

- Experience with Cloud Computing technologies like AWS/Azure/GCP and/or Docker/Kubernetes

- Ability to produce clear, detailed project scoping and design documentation to be used by technical specialists to build appropriate solutions for Altair Knowledge Works customers

- Well versed in generic administration tasks of managing Docker images, container networking and standard infrastructure maintenance tasks on Docker and Kubernetes platform

- Built and managed a container orchestration platform from the ground up

- Experience with managing Docker Swarm

- Experience in managing a large K8S cluster and scaling it significantly at enterprise scale

- Experience with monitoring tools such as Grafana, and Kibana

- Working experience/conceptual knowledge in BI/DWH/ETL concepts and tools

- Professional Services/Consulting and client facing experience

- Excellent communication skills, both written and oral

:::
:::{tab-item} Team Lead


- Not applicable

:::
::::

:::::

---



::::{grid} 2
:gutter: 1

:::{grid-item-card} Lead Technical Support Engineer
:text-align: center 
{octicon}`person`

:::
:::{grid-item-card} Team Lead - Technical Support Engineer
:text-align: center
{octicon}`people`
:::
::::

:::::{dropdown} Level 4 - Lead
:animate: fade-in-slide-down
:color: info
:margin: 0

::::{tab-set}
:::{tab-item} Overview


- Increased knowledge of single or multiple products/offerings, expert in either a single product or a module, or good knowledge of multiple products/offerings

- Good understanding of the engineering problems we can provide solutions to and the product interdependencies

- Ability to work on more than one functional area, product, and solutions

- Create and contributes to solution direction

- Gain understanding of our products and solutions as used in the industry

- Gain an understanding of the best practices in a product/solution or industry

:::
:::{tab-item} Responsibilities


- Mentor, train, and guide interns/ junior team members

- Contribute to technical trainings on advanced topics/products/solutions

- Help in technical documentation in the form of digital content, knowledge base articles and be active on Altair community

- Coordinate and collaborate smoothly across multiple internal and external cross- functional teams

- Helping our customers be successful with Altair Data Analytics products by answering questions, assisting with installations, troubleshooting issues and providing workarounds.

- Identifying possible solutions to a variety of technical problems and takes appropriate action to resolve quickly.

- Quickly develop a deep understanding of Altair Data Analytics products and dependent technologies.

- Creating Knowledgebase articles (FAQ/Solutions) that provide technical value to the Altair Data Analytics customer base.

- Ability to work on multiple priorities simultaneously.

- Monitoring and responding to end user inquiries via telephone, web, chat, email and online forums.

- Excellent communication skills, especially telephone skills

- Mentoring junior level engineers on support processes and problem analysis skillset.

- Assist the manager with monitoring overall case response times and SLA achievement, verifying customers are contacted in line with internal guidelines

- Review cases periodically to ensure appropriate procedures and escalation paths are followed by team members.

- Provide periodic updates to Customer Support Manager on engineer performance, potential issues, and opportunities for improved efficiency.

- Serve as formal escalation point within the team for difficult cases

- Assist with growing the skills of individual team members, providing feedback on appropriate case handling, and troubleshooting strategies.

- Work with management to help identify support trends and opportunities for improvement within the team.

- Provide training to new hires and ongoing skill-up training for existing employees. Ensure engineers are attending relevant training sessions and participating in enablement sessions.

:::
:::{tab-item} Qualifications


- Bachelor's, Master's or PhD in Computer Science, Engineering, Mathematics, Data Science or a related technical field

- Good understanding of multiple products in Altair’s offerings

- Exposure/Knowledge of DA tools and the workflows in different industry verticals

- Exposure to other data analytics solutions/products (open-source, Alteryx, RapidMiner, Dataiku, etc.) is preferred

- Ability to collaborate with all levels of technical users and sales teams

- Driven individual, excited to learn new technology

- Strong Interpersonal and communication skills (oral and written)

- Team player with ability to work independently as well as collaboratively

:::
:::{tab-item} Skills


- For in-depth technical skill matrix, refer to the appendix

:::
:::{tab-item} Team Lead


- Increased knowledge multiple products/our offerings, expert in multiple products/modules

- In-depth understanding of the solutions we can provide and the product interdependencies in the technical area of interest

- Good understanding of our products and solutions as used in the industry

- Input in defining solutions and driving technical direction in customer engagements

- Ability to work on more than one functional area, product, and solutions

- Gain knowledge of the value of our product offerings and how to position them

- Input in defining the best practices/solutions in an industry, publish papers/articles on the same

- Gain understanding of upcoming areas in different industry verticals and be able to conceptualize forward thinking solutions

- Communicates progress to various stake holders

- Lead and manage a world-class team by hiring exceptional talent, coaching, and developing engineers, and leveraging your technical expertise to constantly prioritize project activities for the team

- Manages technical resources between various project activities

- Responsible for developing and tracking timelines for project activities and the ROI on the activities

:::
::::

:::::

---



::::{grid} 2
:gutter: 1

:::{grid-item-card} Support Solution Engineer<br />- or -<br />Sr. Support Solution Engineer
:text-align: center 
{octicon}`person`

:::
:::{grid-item-card} Manager - Technical Support<br />- or -<br />Sr. Manager Technical Support
:text-align: center
{octicon}`people`
:::
::::

:::::{dropdown} Level 5 - Manager
:animate: fade-in-slide-down
:color: info
:margin: 0

::::{tab-set}
:::{tab-item} Overview


- Increased knowledge multiple products/our offerings, expert in multiple products/modules 

- In-depth understanding of the solutions we can provide and the product interdependencies in the technical area of interest 

- Good understanding of our products and solutions as used in the industry 

- Input in defining solutions and driving technical direction in customer engagements 

- Ability to work on more than one functional area, product, and solutions 

- Gain knowledge of the value of our product offerings and how to position them 

- Input in defining the best practices/solutions in an industry, publish papers/articles on the same 

- Gain understanding of upcoming areas in different industry verticals and be able to conceptualize forward thinking solutions 

- Communicates progress to various stake holders 

- Lead and manage a world-class team by hiring exceptional talent, coaching, and developing engineers, and leveraging your technical expertise to constantly prioritize project activities for the team 

- Manages technical resources between various project activities 

- Responsible for developing and tracking timelines for project activities and the ROI on the activities 

:::
:::{tab-item} Responsibilities


- Manage Altair Data Analytics Support team

- Assign incoming cases to team, ensuring the resources assigned have the correct abilities and bandwidth to resolve the issue based on case depth and severity.

- Resolve customer issues, implementing and meeting Service Level Agreements and improving our current processes.

- Act as the escalation manager when customer issues severely impact a customer, driving problems to resolution and leading the communications within Altair and with the client.

- Establish relationships with customers, especially key or strategic customers.

- Collaborate with internal teams (Development, Product Management, Quality Assurance, Services) on trending customer issues and ensuring external communications are accurate and clear.

- Develop employee training requirements to ensure staff is highly proficient with Altair DA Products and internal processes.

- Ensure quality of case handling communications, timely responses, and specific care for critical customers.Assess and drive metrics for specific support areas, analyzing the data for improvement ideas.

- Manage Altair Data Analytics consulting team

- Assign incoming projects to team, ensuring the resources assigned have the correct abilities and bandwidth to deliver the project based on project complexity.

- Establish relationships with customers, especially key or strategic customers.

- Collaborate with internal teams (Development, Product Management, Quality Assurance, Support) on trending customer issues and ensuring external communications are accurate and clear.

- Develop employee training requirements to ensure staff is highly proficient with Altair DA Products and internal processes.

- Ensure quality of project handling communications, timely responses, and specific care for critical customers.

- Assess and drive metrics for specific consulting areas, analyzing the data for improvement ideas.

:::
:::{tab-item} Qualifications


- TBD

:::
:::{tab-item} Skills


- For in-depth technical skill matrix, refer to the appendix

:::
:::{tab-item} Team Lead


:::
::::

:::::

---



::::{grid} 2
:gutter: 1

:::{grid-item-card} Principal Support Solution Engineer<br />- or -<br />Sr. Principal Support Solution Engineer
:text-align: center 
{octicon}`person`

:::
:::{grid-item-card} Director - Technical Support<br />- or -<br />Sr. Director Technical Support
:text-align: center
{octicon}`people`
:::
::::

:::::{dropdown} Level 6 - Director
:animate: fade-in-slide-down
:color: info
:margin: 0

::::{tab-set}
:::{tab-item} Overview


- Increased knowledge multiple products/our offerings, expert in multiple products/modules

- In-depth understanding of the solutions we can provide and the product interdependencies in the technical area of interest

- Good understanding of our products and solutions as used in the industry

- Input in defining solutions and driving technical direction in customer engagements

- Ability to work on more than one functional area, product, and solutions

- Gain knowledge of the value of our product offerings and how to position them

- Input in defining the best practices/solutions in an industry, publish papers/articles on the same

- Gain understanding of upcoming areas in different industry verticals and be able to conceptualize forward thinking solutions

- Communicates progress to various stake holders

- Lead and manage a world-class team by hiring exceptional talent, coaching, and developing engineers, and leveraging your technical expertise to constantly prioritize project activities for the team

- Manages technical resources between various project activities

- Responsible for developing and tracking timelines for project activities and the ROI on the activities

:::
:::{tab-item} Responsibilities


- TBD

:::
:::{tab-item} Qualifications


- TBD

:::
:::{tab-item} Skills


- For in-depth technical skill matrix, refer to the appendix

:::
:::{tab-item} Team Lead


- TBD

:::
::::

:::::

---



::::{grid} 2
:gutter: 1

:::{grid-item-card} Technical Fellow
:text-align: center 
{octicon}`person`

:::
:::{grid-item-card} Vice President - Global Technical Team
:text-align: center
{octicon}`people`
:::
::::

:::::{dropdown} Level 7 - Vice President
:animate: fade-in-slide-down
:color: info
:margin: 0

::::{tab-set}
:::{tab-item} Overview


- TBD

:::
:::{tab-item} Responsibilities


- TBD

:::
:::{tab-item} Qualifications


- TBD

:::
:::{tab-item} Skills


- For in-depth technical skill matrix, refer to the appendix

:::
:::{tab-item} Team Lead


- TBD

:::
::::

:::::

---



::::{grid} 2
:gutter: 1

:::{grid-item-card} None
:text-align: center 
{octicon}`person`

:::
:::{grid-item-card} Sr. Vice President - Global Technical Team
:text-align: center
{octicon}`people`
:::
::::

:::::{dropdown} Level 8 - Sr. Vice President
:animate: fade-in-slide-down
:color: info
:margin: 0

::::{tab-set}
:::{tab-item} Overview


- TBD

:::
:::{tab-item} Responsibilities


- TBD

:::
:::{tab-item} Qualifications


- TBD

:::
:::{tab-item} Skills


- For in-depth technical skill matrix, refer to the appendix

:::
:::{tab-item} Team Lead


- TBD

:::
::::

:::::

---



[Home](home)