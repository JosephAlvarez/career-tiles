#!/bin/bash


shopt -s expand_aliases

export output_directory="/Users/laruebrown/Developer/career-tiles"
export data_directory="/Users/laruebrown/Downloads/Career Tiles/tiles"

# senvironment setup
export working_directory=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
alias python=python3

# rcreate new markdown files
cd ${working_directory}
for directory in $( find ./docs -type d -mindepth 1 -and -not -name "images*" )
do
        rm -rf $directory
done
python create_tiles.py 

# build html from markdown file
cd ${working_directory}
rm -rf ./public
sphinx-build -b html ./docs ./public

# convert html to aspx
mkdir -p "${output_directory}/aspx"
cp -r "${working_directory}/public/"* "${output_directory}/aspx"
cp "${working_directory}/convert_to_aspx.py" "${output_directory}/aspx"
cd "${output_directory}/aspx"
python convert_to_aspx.py
rm -rf convert_to_aspx.py

# return to current directory
cd ${working_directory}