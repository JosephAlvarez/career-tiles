from jinja2 import Environment, FileSystemLoader
from pathlib import Path
import pandas as pd
import datetime
import os
import re

root_directory = os.getenv('PWD')
docs_directory = f'{root_directory}/docs'
data_directory = os.getenv('data_directory')
padding = '#' * 100

environment = Environment(loader = FileSystemLoader(f'{root_directory}/templates/'))

print(f'{padding}\n{padding}\nCreating Career Tiles from Template...\n{padding}\n{padding}\n\n')

for file_name in os.listdir(f'{data_directory}'):

    if file_name in ['skills_matrix.xlsx', 'template.xlsx']:
        print(f'\n{file_name} was skipped\n')
        continue

    # Excel pre-processing / data cleaning
    file_path = f'{data_directory}/{file_name}'
    try:
        metadata = pd.read_excel(open(file_path, 'rb'), sheet_name = 'Metadata')
        titles = pd.read_excel(open(file_path, 'rb'), sheet_name = 'Titles')
        overview = pd.read_excel(open(file_path, 'rb'), sheet_name = 'Overview')
        responsibilities = pd.read_excel(open(file_path, 'rb'), sheet_name = 'Responsibilities')
        qualifications = pd.read_excel(open(file_path, 'rb'), sheet_name = 'Qualifications')
        skills = pd.read_excel(open(file_path, 'rb'), sheet_name = 'Skills')
        team_lead = pd.read_excel(open(file_path, 'rb'), sheet_name = 'Team Lead Responsibilities')
    except:
        print(f'{file_name} was skipped')
        continue

    levels = [1, 2, 3, 4, 5, 6, 7, 8, 9]

    modification_date = os.path.getmtime(file_path)
    modification_date = datetime.datetime.fromtimestamp(modification_date).strftime('%B %d, %Y')
    group = metadata.loc[0]['Group']
    sub_group = metadata.loc[0]['Sub-Group']
    page_title = metadata.loc[0]['Page Title']

    # function to convert description data frames into dictionaries
    def convert_to_dict(dataframe:pd.DataFrame) -> dict():
        # remove blank and invalid items from dataframe
        dataframe = dataframe.loc[dataframe['Validation'] == 'Valid']
        dictionary = dict()

        # for each level, assign level number as key
        # and add empty list as the value
        for level in levels:
            dictionary[level] = list()

        # iterate over the dataframe, and append each "Item"
        # to the appropriate list based on the level
        for index, row in dataframe.iterrows():
            dictionary[int(row['Level'])].append(row['Item'])

        # return the completed dictionary
        return dictionary

    overview = convert_to_dict(overview)
    responsibilities = convert_to_dict(responsibilities)
    skills = convert_to_dict(skills)
    qualifications = convert_to_dict(qualifications)
    team_lead = convert_to_dict(team_lead)

    # convert titles to a list of lists
    titles = titles.loc[(titles['Validation'] == 1), (titles.columns != 'Validation')].values.tolist()

    # fill in template
    
    tiles_template = environment.get_template('tiles.md')

    tiles_content = tiles_template.render(
        tile_name = page_title,
        titles = titles,
        overview = overview,
        responsibilities = responsibilities,
        skills = skills,
        qualifications = qualifications,
        team_lead = team_lead,
        modification_date = modification_date
    )

    updated_page_title = re.sub(r'[^a-zA-Z ]', '', page_title).replace(' ', '_').lower()
    output_file = f'{group}-{sub_group}-{updated_page_title}.md'
    tile_output_path = f'{docs_directory}/{group}/{sub_group}'
    if not os.path.exists(tile_output_path):
        os.makedirs(tile_output_path)
    
    with open(f'{tile_output_path}/{output_file}', mode = 'w', encoding = 'utf-8') as tile:
        tile.write(tiles_content)
        print(f'{padding}\n{output_file} was successfully created\n{padding}\n')

    index_template = environment.get_template('index.md')
    index_content = index_template.render(group_name = f'{group.upper()}')
    index_output_path = f'{docs_directory}/{group}/index.md'

    if not os.path.exists(index_output_path):
        with open(index_output_path, mode = 'w', encoding = 'utf-8') as index:
            index.write(index_content)

    sub_index_template = environment.get_template('_index.md')
    sub_index_content = sub_index_template.render(group_name = f'{sub_group.upper()}')
    sub_index_output_path = f'{tile_output_path}/_index.md'

    if not os.path.exists(sub_index_output_path):
        with open(sub_index_output_path, mode = 'w', encoding = 'utf-8') as sub_index:
            sub_index.write(sub_index_content)